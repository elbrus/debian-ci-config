# Debian Continuous Integration

This repository contains configuration files for the Debian Continuous
Integration service (i.e. ci.debian.net). For debci (the code that runs the
system), see:

https://salsa.debian.org/ci-team/debci

The configuration here uses [Chef](https://docs.chef.io/) and can be applied to
the nodes with [chake](https://gitlab.com/terceiro/chake).

## Overview of the repository organization

The most important top-level files or directories:

* `config/`: node configuration (hostnames, roles, IPs, etc). Note that the
  configuration is keyed by environment. we currently have 3 environments
  defined: development, staging, and production. development points to local
  vagrant VMs, and staging and production to Amazon EC2 instances.
* `cookbooks/`: the chef configuration cookbooks
* `roles/`: definition of roles for the nodes. atm we have only two roles:
  master and worker.
* `rakelib/`: rake tasks specific to Debian CI
* `whitelists/`: links to the existing package whitelists
* `blacklist`: package blacklist

## Cheat sheet

Before anything, you need to know that `$ENV` must be set to the name of a
subdirectory of `config/`. This will determine which set of nodes will be acted
upon.  If unset, `$ENV` defaults to `development`.

There are several actions that are general for chake repositories.  See the
[chake documentation](https://gitlab.com/terceiro/chake) for general
interacting with the nodes (applying chef configuration, running commands on
all nodes, logging in to nodes, etc).

Below are some tips on common issues that need to be solved in the Debian CI
infrastructure.

### stop the scheduling of tests on unstable

This is needed to stop new tests from being scheduled. But note that there will
still be jobs in the queue, so if you need to do a planned downtime, you should
probably stop the scheduling of new tests at least 24h before that.

```
$ rake batch:offline ENV=production
```

### start the schedule of tests on unstable

After some planned maintainance, to start the scheduling of tests again:

```
$ rake batch:online ENV=production
```

### managing worker daemons

Sometimes there are issues that are specific to some worker node. First login
to that node (let's say `ci-worker01`):

```
$ rake login:ci-worker01.debian.net
```

From there, stop the worker:

```
$ sudo systemctl stop debci-worker
```

After solving the issue, start it again:

```
$ sudo systemctl start debci-worker
```
