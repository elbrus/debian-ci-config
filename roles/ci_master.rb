name 'ci_master'
description 'CI central server'
run_list *%w[
  recipe[basics]
  recipe[debian_sso]
  recipe[munin::node]
  recipe[debci::ssl]
  recipe[debci]
  recipe[debci::master]
  recipe[munin]
]

