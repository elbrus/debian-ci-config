name 'ci_worker'
description 'CI worker'
run_list *%w[
  recipe[basics]
  recipe[munin::node]
  recipe[debci]
  recipe[debci::worker]
]


