name 'openvpn_client'
description 'OpenVPN client'
run_list %w[
  recipe[openvpn::client]
]
