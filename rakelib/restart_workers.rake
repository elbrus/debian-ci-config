desc 'restarts all workers'
task :restart_workers do
  Rake::Task['run'].invoke('dpkg-query --show debci-worker && sudo systemctl restart debci-worker || true')
end
