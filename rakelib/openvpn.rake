task :keys do
  needed = $nodes.select do |node|
    run_list = node.data['run_list']
    run_list && run_list.include?('role[openvpn_client]') && !File.exist?("cookbooks/openvpn/ca/keys/#{node.hostname}.key.asc")
  end

  unless needed.empty?
    chdir "cookbooks/openvpn/ca" do
      sh "./decrypt"
      needed.each do |node|
        sh ". ./vars && ./pkitool #{node.hostname}"
      end
      sh './encrypt'
    end
  end
  chdir "cookbooks/openvpn/ca" do
    sh './install'
  end
end
