desc 'list public and private IPS'
task :ips do
  data = YAML.load_file('config/production/nodes.yaml')
  data.each do |node,data|
    if data['public_ip']
      printf("%s\t%s\t%s\n", node, data['public_ip'], data['internal_ip'])
    end
  end
end
