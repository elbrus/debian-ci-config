$master_node = $nodes.find { |node| node.data['run_list'].include?('role[ci_master]') }
$architectures = $master_node.data['debci']['architectures']

def batch_command(cmd)
  $architectures.map do |arch|
    cmd.gsub('{ARCH}', arch)
  end.join(' ; ')
end

namespace :batch do
  desc 'Puts the debci batch processors offline'
  task :offline do
    Rake::Task['run:' + $master_node.hostname].invoke(batch_command('sudo systemctl stop debci-batch-{ARCH}.timer'))
  end

  desc 'Brings the debci batch processors online'
  task :online do
    Rake::Task['run:' + $master_node.hostname].invoke(batch_command('sudo systemctl start debci-batch-{ARCH}.timer'))
  end

  desc 'Checks the status of the debci batch processors'
  task :status do
    Rake::Task['run:' + $master_node.hostname].invoke(batch_command('sudo systemctl status debci-batch-{ARCH}.timer debci-batch-{ARCH}.service') + '; sudo systemctl list-timers')
  end
end
