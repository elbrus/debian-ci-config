desc 'Upgrade all hosts (only CI components)'
task :upgrade do
  Rake::Task['run'].invoke('sudo apt-get update')
  Rake::Task['converge'].invoke
end

desc 'Runs `apt-get dist-upgrade` on all hosts'
task :dist_upgrade do
  Rake::Task['run'].invoke('sudo apt-get update; sudo DEBIAN_FRONTEND=noninteractive apt-get -qy dist-upgrade')
  Rake::Task['converge'].invoke
end
