unless Chake::VERSION >= '0.15'
  fail "chake version 0.15 or higher required (you have #{Chake::VERSION})"
end

node_files = Dir.glob("#{ENV["CHAKE_NODES"]}") + Dir.glob(ENV["CHAKE_NODES_D"] + '/*.yaml')

if $nodes.map { |node| node.data['public_ip'] || node.data['ssh_options'] }.compact.size > 0
  ssh_config = File.join('config', $ENV, 'ssh_config')
  ENV['CHAKE_SSH_CONFIG'] = ssh_config
  task :connect_common => ssh_config
  file ssh_config => node_files do |t|
    File.open(t.name, 'w') do |f|
      $nodes.each do |node|
        ssh_options = Array(node.data['ssh_options'])
        if ssh_options.empty?
          ssh_options << "HostName #{node.data['public_ip']}" if node.data['public_ip']
          ssh_options << "User admin"
        end

        f.puts "Host #{node.hostname}"
        ssh_options.each do |option|
          f.puts "  " + String(option).strip
        end

      end
    end
    puts "#{t.name} ← #{t.prerequisites.join(', ')}"
  end
end
