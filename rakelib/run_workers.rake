workers = $nodes.select { |node| node.data['run_list'].include?('role[ci_worker]') }

multitask 'run:workers' => workers.map { |w| "run:#{w.hostname}" }
