desc 'generate entries for debian.net DNS'
task :dns do
  $nodes.sort_by(&:hostname).each do |node|
    printf(
      "%-11s in a      %s\n",
      node.hostname.sub('.debian.net', ''),
      node.data['public_ip']
    )
  end
end
