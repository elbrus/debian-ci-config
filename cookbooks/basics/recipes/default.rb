directory '/home/admin/.ssh' do
  user    'admin'
  group   'admin'
  mode    0700
end

cookbook_file '/home/admin/.ssh/authorized_keys' do
  user    'admin'
  group   'admin'
  mode    0600
end

template '/etc/apt/sources.list' do
  variables mirror: node['mirror']
  notifies :run, 'execute[apt-get update]', :immediately
end

execute 'apt-get update' do
  action :nothing
end

cookbook_file '/etc/apt/apt.conf.d/00updates' do
  source 'apt.conf'
end

# useful tools
package 'ack-grep'
package 'bash-completion'
package 'fail2ban'
package 'htop'
package 'iotop'
package 'iptraf-ng'
package 'tmux'
package 'vim'
package 'pastebinit'
package 'virt-what'
