directory '/etc/debci'
directory '/etc/debci/conf.d'

file '/etc/debci/conf.d/00_staging.conf' do
  content "debci_backend=fake\n"
end

file '/etc/apt/sources.list.d/debci-next.list' do
  content "deb https://people.debian.org/~terceiro/debci-next/ ./"
end
