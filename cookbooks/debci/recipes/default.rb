package 'apt-transport-https'

cookbook_file '/etc/apt/trusted.gpg.d/debci.gpg' do
  source 'debci.gpg.bin'
end

template '/etc/apt/sources.list.d/debci.list' do
  variables mirror: node['mirror']
  notifies :run, 'execute[apt-get update]', :immediately
end

directory '/etc/debci/conf.d' do
  recursive true
end
file "/etc/debci/conf.d/00_backend.conf" do
  content "debci_backend=lxc\n"
end

if node['debci'] && node['debci']['suites']
  file '/etc/debci/conf.d/01_suite_list.conf' do
    content "debci_suite_list='%s'\n" % node['debci']['suites'].join(' ')
  end
end
