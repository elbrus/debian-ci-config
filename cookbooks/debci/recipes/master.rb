if node['web_hostname']
  file "/etc/debci/conf.d/00_url_base.conf" do
    content "debci_url_base=https://%s\n" % node['web_hostname']
  end
end

if node['debci'] && node['debci']['architectures']
  file '/etc/debci/conf.d/00_arch_list.conf' do
    content "debci_arch_list='%s'\n" % node['debci']['architectures'].join(' ')
  end
end

package 'postgresql'
package 'ruby-pg'

execute 'createuser' do
  user 'postgres'
  command 'createuser debci || true'
end

execute 'createdb' do
  user 'postgres'
  command 'createdb --owner=debci debci || true'
end

file '/etc/debci/conf.d/00_database.conf' do
  content "debci_database_url='postgresql:///debci'"
end

cookbook_file '/etc/debci/blacklist'
cookbook_file '/etc/debci/whitelist' do
  mode 0755
end
cookbook_file '/etc/debci/whitelist-perl.txt'
cookbook_file '/etc/debci/whitelist-ruby.txt'
cookbook_file '/etc/debci/whitelist-go.txt'
cookbook_file '/etc/debci/whitelist-python.txt'

package 'rabbitmq-server'
service 'rabbitmq-server'
cookbook_file '/etc/munin/plugins/debci_queue_size' do
  mode 0755
  notifies :restart, 'service[munin-node]'
end
file '/etc/munin/plugin-conf.d/debci_queue_size' do
  content ['[debci_queue_size]', 'user root'].join("\n") + "\n"
  notifies :restart, 'service[munin-node]'
end

cookbook_file '/etc/munin/plugins/debci_total_packages_processed' do
  mode 0755
  notifies :restart, 'service[munin-node]'
end
file '/etc/munin/plugin-conf.d/debci_total_packages_processed' do
  content ['[debci_total_packages_processed]', 'user root'].join("\n") + "\n"
  notifies :restart, 'service[munin-node]'
end

file '/etc/rabbitmq/rabbitmq-env.conf' do
  content "RABBITMQ_NODE_IP_ADDRESS=0.0.0.0\n"
  notifies :restart, 'service[rabbitmq-server]'
end

cookbook_file '/etc/rabbitmq/rabbitmq.config' do
  notifies :restart, 'service[rabbitmq-server]'
end

package 'debci-collector' do
  action :upgrade
end

package 'apache2'
package 'libapache2-mod-passenger'
execute 'a2enmod headers'
execute 'a2enmod ssl'
service 'apache2' do
  supports :reload => true
end
template '/etc/apache2/sites-available/debci.conf' do
  source 'apache.conf.erb'
  notifies :reload, 'service[apache2]'
end
execute 'a2ensite debci.conf'
