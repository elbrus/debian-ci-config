package 'ssl-cert'

site = node['web_hostname']

if site == 'debci-master.local'
  directory '/var/lib/dehydrated'
  directory '/var/lib/dehydrated/certs'
  directory '/var/lib/dehydrated/certs/debci-master.local'
  cookbook_file '/var/lib/dehydrated/certs/debci-master.local/cert.pem'
  cookbook_file '/var/lib/dehydrated/certs/debci-master.local/fullchain.pem'
  cookbook_file '/var/lib/dehydrated/certs/debci-master.local/privkey.pem'
else
  package 'dehydrated'
  package 'dehydrated-apache2'

  file "/etc/dehydrated/domains.txt" do
    user      'root'
    group     'root'
    mode      '0640'
    content   "#{site}\n"
    notifies  :run, 'execute[dehydrated]'
  end

  cookbook_file '/etc/systemd/system/dehydrated.service' do
    user      'root'
    group     'root'
    mode      '0640'
    notifies  :run, 'execute[install dehydrated service]'
  end

  cookbook_file '/etc/systemd/system/dehydrated.timer' do
    user      'root'
    group     'root'
    mode      '0640'
    notifies  :run, 'execute[install dehydrated timer]'
  end

  execute 'install dehydrated service' do
    command   'systemctl daemon-reload'
    action    :nothing
  end

  execute 'install dehydrated timer' do
    command   'systemctl daemon-reload && systemctl enable dehydrated.timer && systemctl start dehydrated.timer'
    action    :nothing
  end

  execute 'dehydrated' do
    command   'systemctl start dehydrated.service'
    action    :nothing
  end

end
