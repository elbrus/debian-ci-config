cookbook_file '/etc/apt/preferences.d/debci-worker-pinning'
package 'autodep8' do
  action :upgrade
end
package 'autopkgtest' do
  action :upgrade
end

package 'lxc'
package 'libvirt-clients'
package 'libvirt-daemon-system'
execute 'libvirt-bridge-setup' do
  not_if 'virsh net-list | grep -q default'

  commands = [
    'virsh net-start default',
    'virsh net-autostart default',
  ]
  command  commands.join(' && ')
end
template '/etc/lxc/default.conf' do
  source  'lxc.conf.erb'
end

package 'debci-worker' do
  action :upgrade
end
service 'debci-worker' do
  provider Chef::Provider::Service::Systemd
end

if node['debci']['amqp_server']
  file '/etc/debci/conf.d/00_amqp_server.conf' do
    content "#test\ndebci_amqp_server=#{node['debci']['amqp_server']}\n"
    notifies :restart, 'service[debci-worker]'
  end
end

file '/etc/debci/conf.d/00_mirror.conf' do
  content "export MIRROR=#{node['mirror']}\n"
end

%w[
  debci_packages_processed
  debci_packages_being_processed
  debci_containers
].each do |munin_plugin|
  cookbook_file "/etc/munin/plugins/#{munin_plugin}" do
    mode 0755
    notifies :restart, 'service[munin-node]'
  end
  file "/etc/munin/plugin-conf.d/#{munin_plugin}" do
    content ["[#{munin_plugin}]", 'user root'].join("\n") + "\n"
    notifies :restart, 'service[munin-node]'
  end
end
