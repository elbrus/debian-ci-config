package 'python3'
package 'python3-requests'

cookbook_file '/usr/local/bin/update-debsso-ca' do
  mode 0755
end

directory '/srv/sso.debian.org/etc' do
  recursive true
end

execute '/usr/local/bin/update-debsso-ca --destdir /srv/sso.debian.org/etc' do
  not_if 'test -f /srv/sso.debian.org/etc/debsso.crt'
end

file '/etc/cron.d/update-debsso-ca' do
  action :delete
end

cookbook_file '/etc/systemd/system/update-debsso-ca.service' do
  notifies :run, 'execute[systemd-enable-update-debsso-ca]'
end

cookbook_file '/etc/systemd/system/update-debsso-ca.timer' do
  notifies :run, 'execute[systemd-enable-update-debsso-ca]'
end

execute 'systemd-enable-update-debsso-ca' do
  command 'systemctl daemon-reload && systemctl start update-debsso-ca.timer'
  action :nothing
end
