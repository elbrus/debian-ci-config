include_recipe 'openvpn'

file '/etc/openvpn/tun0.conf' do
  action :delete
end

cookbook_file "/etc/openvpn/dh2048.pem" do
  notifies :run, 'execute[openvpn-restart]'
  mode 0644
end

template '/etc/openvpn/server.conf' do
  source 'server.conf.erb'
  mode 0644
  notifies :run, 'execute[openvpn-restart]'
end

directory '/etc/openvpn/client-config'

network = node['openvpn_server']['network'].split('.').first(3)
node['hosts'].each do |hostname,ip|
  if ip.split('.').first(3) == network
    # handle only clients in the VPN network
    peer = (ip.split('.').first(3) + [(ip.split('.').last.to_i - 1).to_s]).join('.')
    file "/etc/openvpn/client-config/#{hostname}" do
      content "ifconfig-push #{ip} #{peer}\n"
      notifies :run, 'execute[openvpn-restart]'
    end
  end
end
