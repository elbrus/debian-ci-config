package 'openvpn'
service 'openvpn'

file '/etc/openvpn/static.key' do
  action :delete
end

[
  "/etc/openvpn/ca.crt",
  "/etc/openvpn/#{node["fqdn"]}.crt",
  "/etc/openvpn/#{node["fqdn"]}.key",
].each do |f|
  cookbook_file f do
    mode 0400
    notifies :run, 'execute[openvpn-restart]'
  end
end

role = node['openvpn_server'] && 'server' || 'client'
execute 'openvpn-restart' do
  command 'systemctl restart openvpn@%s' % role
  action :nothing
end
