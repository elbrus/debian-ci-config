include_recipe 'openvpn'

file '/etc/openvpn/tun0.conf' do
  action :delete
end

template '/etc/openvpn/client.conf' do
  source 'client.conf.erb'
  mode 0644
  notifies :run, 'execute[openvpn-restart]'
end

