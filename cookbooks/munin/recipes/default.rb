package 'munin'

file '/etc/munin/munin.conf' do
  content "includedir /etc/munin/munin-conf.d\n"
end

template '/etc/munin/munin-conf.d/hosts.conf'
